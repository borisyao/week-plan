# weekPlan

## Class Diagram

![update](https://gitlab.istic.univ-rennes1.fr/byao/weekplan/-/wikis/uploads/3ccdce77419e994fe2594c113f62cf82/diagramme.png)

## Development

To start application in the dev profile

    ./mvnw

## Building App

### Packaging as jar

    ./mvnw -Pprod clean verify


### Packaging as war

    ./mvnw -Pprod,war clean verify



### Code quality

```
docker-compose -f src/main/docker/sonar.yml up -d
```

Then, run a Sonar analysis:

```
./mvnw -Pprod clean verify sonar:sonar
```


## Using Docker


to start a mysql database in a docker container, run:

    docker-compose -f src/main/docker/mysql.yml up -d

build a docker image of app by running:

    ./mvnw -Pprod verify jib:dockerBuild

Run App

    docker-compose -f src/main/docker/app.yml up -d

