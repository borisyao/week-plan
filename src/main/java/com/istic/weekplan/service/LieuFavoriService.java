package com.istic.weekplan.service;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.istic.weekplan.domain.ActivitePerso;
import com.istic.weekplan.domain.Lieu;
import com.istic.weekplan.repository.LieuRepository;
import com.istic.weekplan.service.dto.LieuDTO;
import com.istic.weekplan.web.rest.errors.ApAlreadyUsedException;

@Service
@Transactional
public class LieuFavoriService {
	
    private final Logger log = LoggerFactory.getLogger(LieuFavoriService.class);
    
    private final LieuRepository lieuRepository;

    public LieuFavoriService(LieuRepository lieuRepository) {
        this.lieuRepository = lieuRepository;
    }

	public Lieu ajoutLieuFav(@Valid LieuDTO lieuDTO) {
    	if ((lieuRepository.findAllLieu()).contains(lieuDTO)) {
    		throw new ApAlreadyUsedException();
    	}
        Lieu newLieu = new Lieu();
        newLieu.setDepartement(lieuDTO.getDepartement());
        newLieu.setRegion(lieuDTO.getRegion());
        newLieu.setVille(lieuDTO.getVille());
        newLieu.setConditionMeteo(lieuDTO.getConditionMeteo());
        lieuRepository.save(newLieu);
        log.debug("Created Lieu: {}", newLieu);
        return newLieu;
	}

}
