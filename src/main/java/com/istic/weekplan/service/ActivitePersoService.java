package com.istic.weekplan.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.istic.weekplan.domain.ActivitePerso;
import com.istic.weekplan.domain.Personne;
import com.istic.weekplan.repository.ActivitePersoRepository;
import com.istic.weekplan.repository.AuthorityRepository;
import com.istic.weekplan.repository.PersonneRepository;
import com.istic.weekplan.security.SecurityUtils;
import com.istic.weekplan.service.dto.ActivitePersoDTO;
import com.istic.weekplan.service.dto.PersonneDTO;
import com.istic.weekplan.web.rest.errors.ApAlreadyUsedException;
import com.istic.weekplan.web.rest.errors.EmailAlreadyUsedException;
import com.istic.weekplan.web.rest.errors.InvalidPasswordException;

@Service
@Transactional
public class ActivitePersoService {
	
    private final Logger log = LoggerFactory.getLogger(ActivitePersoService.class);
    
    private final ActivitePersoRepository activitePersoRepository;

    public ActivitePersoService(ActivitePersoRepository activitePersoRepository) {
        this.activitePersoRepository = activitePersoRepository;
    }
    
    public ActivitePerso ajoutActivite(ActivitePersoDTO activitePersoDTO) {
    	if ((activitePersoRepository.findAllActivitePerso()).contains(activitePersoDTO)) {
    		throw new ApAlreadyUsedException();
    	}
        ActivitePerso newActivitePerso = new ActivitePerso();
        newActivitePerso.setActivite(activitePersoDTO.getActivite());
        newActivitePerso.setPersonne(activitePersoDTO.getPersonne());
        newActivitePerso.setLieux(activitePersoDTO.getLieux());
        newActivitePerso.setConditionMeteo(activitePersoDTO.getConditionMeteo());
        activitePersoRepository.save(newActivitePerso);
        log.debug("Created Activite Perso: {}", newActivitePerso);
        return newActivitePerso;
    }
    
}
