package com.istic.weekplan.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.istic.weekplan.domain.Personne;

@Repository
public interface PersonneRepository extends CrudRepository<Personne, String>{
	
	Optional<Personne> findOneByEmail(String email);
	
    Optional<Personne> findOneByLogin(String login);
	
	//TODO does this work?
	Optional<List<Personne>> findByEmailAndActivitesPersoNotNull(String email);
	
	Optional<Personne> findOneByActivitesPersoId(long id);
	
	Optional<List<Personne>> findByLieuxId(long id);

	Optional<Personne> findOneByEmailIgnoreCase(String email);
	
}
