package com.istic.weekplan.web.rest.errors;

public class ApAlreadyUsedException extends BadRequestAlertException{

    private static final long serialVersionUID = 1L;

    public ApAlreadyUsedException() {
        super(ErrorConstants.AP_ALREADY_USED_TYPE, "Activite Perso is already saved!", "userManagement", "emailexists");
    }
}
